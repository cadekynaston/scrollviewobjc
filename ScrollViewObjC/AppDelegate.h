//
//  AppDelegate.h
//  ScrollViewObjC
//
//  Created by Cade Kynaston on 8/30/16.
//  Copyright © 2016 Cade Kynaston. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

