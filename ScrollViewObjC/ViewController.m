//
//  ViewController.m
//  ScrollViewObjC
//
//  Created by Cade Kynaston on 8/30/16.
//  Copyright © 2016 Cade Kynaston. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //create the initial scrollview
    UIScrollView* myScrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    myScrollView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:myScrollView];
    
    
    myScrollView.contentSize = CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height * 3);
    
    // array of colors for each view
    NSArray *colors = @[[UIColor cyanColor], [UIColor purpleColor], [UIColor redColor], [UIColor blackColor], [UIColor blueColor], [UIColor magentaColor], [UIColor orangeColor], [UIColor greenColor], [UIColor yellowColor]];
    
    //frame will be the number of each frame going left to right
    int frame = 0;
    
    //the frame integer will be cast as a string
    NSString* frameString = @"";
    
    
    //main loop adding each view
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            
            //find the origin for each view. Will be a 3 * 3 grid
            double width = j * self.view.frame.size.width;
            double height = i * self.view.frame.size.height;
            
            //convert each frame integer into a string
            frameString = [NSString stringWithFormat:@"%i", frame + 1];
            
            //set the position of each view
            UIView*  view = [[UIView alloc]initWithFrame:CGRectMake(width, height, self.view.frame.size.width, self.view.frame.size.height)];
            
            //create a label with the number of the frame for each view
            UILabel* lbl = [[UILabel alloc] initWithFrame:(view.bounds)];
            lbl.text = frameString;
            lbl.textColor = [UIColor whiteColor];
            lbl.textAlignment = NSTextAlignmentCenter;
            [lbl setFont:[UIFont systemFontOfSize:55]];
            [view addSubview:lbl];
            
            view.backgroundColor = [colors objectAtIndex:frame];
            
            [myScrollView addSubview:view];
            frame++;
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
