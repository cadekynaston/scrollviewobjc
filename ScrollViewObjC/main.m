//
//  main.m
//  ScrollViewObjC
//
//  Created by Cade Kynaston on 8/30/16.
//  Copyright © 2016 Cade Kynaston. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
